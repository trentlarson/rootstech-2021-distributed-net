## Presentation Steps

Setup Demo 1

- Backup `~/Library/Preferences/dist-task-lists-nodejs/settings.yml` and then copy `settings-demo-1.yml` into it.

- If you're not using this on Trent's system, edit the directory paths in that file (to point to the `distrinet` repo).

- `yarn dev` & clear localStorage `localStorage.removeItem('SAME_IDENTITIES')` & close console

- remove (or comment) one of the metadata from `distrinet/test/features/histories/sample-histories/beverly-hillbillies.html`

Start Slides

Demo 1 (after "Why")

- in genealogy: show private Jed `gedcomx:my-local-test:test-sample-norman#jed`

- log in to FamilySearch.org & retrieve FS Session ID (via [this page](https://www.familysearch.org/platform/)) (or without logging in every time via [this bookmarklet](https://gitlab.com/trentlarson/rootstech-2021-distributed-net/-/raw/master/bookmarklet-get-fssessionid.js) - copy that content directly into a new bookmark)

- show jumping around between public & private data with Norman

- show local histories search, eg. "shoot"

- show "view" highlight

Continue Slides

Demo 2

- `rm ~/Library/Preferences/dist-task-lists-nodejs/settings.yml`

- stop & restart app

- in genealogy: paste in
file:///Users/tlarson/dev/home/distrinet/test/features/genealogy/sample-gedcomx-norman.json#jed

- save data in settings with ID `gedcomx:my-local-test:test-sample-norman`

- add histories for file:///Users/tlarson/dev/home/distrinet/test/features/histories/sample-histories with an ID prefixed `histories:`

- add microdata back into `beverly-hillbillies.html`

- show histories search with ID `gedcomx:my-local-test:test-sample-norman#jed`

- "view" so that it shows a link

- "open" on local filesystem & highlight with bookmarklet

- copy my own settings.yml over, reload, and show more links

Continue Slides

