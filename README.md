# The Global Genealogical Forest with a Distributed Network of Roots

# RootsTech 2021

Full presentation file is [talk-rootstech-2021.pdf](talk-rootstech-2021.pdf).  My own instructions for walking through the steps of the demo are in [presentation-steps.md](presentation-steps.md).

Source code is at [github.com/TrentLarson/distrinet](https://github.com/TrentLarson/distrinet).

- Specifically, this commit: c56c04f879c31535737f226ab9a60f8279b15f85

- Note that I've got my list of next steps in tasks.yml files in there, both specifically for [genealogy](https://raw.githubusercontent.com/trentlarson/distrinet/master/app/features/genealogy/tasks.yml) and also for the overall [Distrinet project](https://raw.githubusercontent.com/trentlarson/distrinet/master/tasks.yml).

The bookmarklets are:

-  bookmarklet-get-fssessionid.js will give your FS Session ID (when logged in)

-  bookmarklet-highlight-urls.js will highlight the URL microdata links in a page to be able to click and explore elsewhere
